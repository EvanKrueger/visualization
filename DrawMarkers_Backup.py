# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
from PIL import Image as im
import os
import scipy.io as sio
import viz
import vizact
import vizshape
#import vizmatplottwo
import scipy
import vizmenu
import time


def CreateTheRoom():

    """<START OF DEFINING THE WORLD>"""
    global BallPlane
    #disables the original light
    viz.disable(viz.LIGHT0)

    topLight = viz.addLight() 
    topLight.setPosition(0,4,0)
    topLight.setEuler( 0, 90 ,0 )
    #topLight.direction(0,-1,0) 
    topLight.spread(270) 
    topLight.intensity(2000) 
    #topLight.spotexponent(2)     
    sideLight = viz.addLight()
    sideLight.setPosition(0,5,0)
    sideLight.setEuler(0,0,90)

    #creates the floor plane to be 35x80
    #rotated around the Z axis(perpendicular to Y axis)
    floor = vizshape.addPlane(size=(35.0,80.0), axis=vizshape.AXIS_Y, cullFace=False)
    #makes the floor look like wood
    floor.texture(viz.addTexture('images/tile_wood.jpg'))
    #moves the floor +20 meters in the z direction
    floor.setPosition(0,0,20)

    """
    Could use Gabe's mocap interaface here to create a room easily
    and play with textures. 

    If performance is an issue, set cullFace to True.
    """
    #adds the wall(plane) farthest from the hand/person
    #35x10 meters**2
    #rotated around the X axis (perpendicular to Z axis)
    frontWall = vizshape.addPlane(size=(35,10), axis=vizshape.AXIS_Z, cullFace = False)
    #moves the wall to match the edge of the 
    frontWall.setPosition(0,5,60)
    #makes the wall appear white
    frontWall.color(viz.WHITE)

    #adds the wall(plane) that when facing the frontWall, is to the camera's left
    #wall is 80x10 meters**2
    #wall is rotated about the Y axis(perpendicular to X axis)
    leftWall = vizshape.addPlane(size=(80,10), axis=vizshape.AXIS_X, cullFace = False)
    #shifts the wall to match the edge of the floor
    leftWall.setPosition(-17.5,5,20)
    #makes the wall appear white
    leftWall.color(viz.WHITE)

    #adds a wall(plane) that when facing the frontWall is to the camera's right
    #wall is 80x10 meters**2
    #wall is rotated about the Y axis(perpendicular to X axis)
    rightWall = vizshape.addPlane(size=(80,10), axis=vizshape.AXIS_X, cullFace = False)
    #shifts the wall to match the edge of the floor
    rightWall.setPosition(17.5,5,20)
    #makes the wall appear white
    rightWall.color(viz.WHITE)

    #adds a wall(plane) that when facing the frontWall is to the camera's back
    #wall is 35x10 meters**2
    #wall is rotated about the X axis(perpendicular to Z axis)
    backWall = vizshape.addPlane(size=(35,10), axis=vizshape.AXIS_Z, cullFace = False)
    #shifts the wall to match the edge of the floor
    backWall.setPosition(0,5,-20)
    #makes the wall appear white
    backWall.color(viz.WHITE)

    #adds a ceiling(plane) that when facing the frontWall is to the camera's topside
    #wall is 35x80 meters**2
    #wall is rotated about the Z axis(perpendicular to Y axis)
    ceiling = vizshape.addPlane(size=(35.0,80.0), axis=vizshape.AXIS_Y, cullFace=False)
    #makes the ceiling appear Skyblue in color
    ceiling.color(viz.SKYBLUE)
    #shifts the ceiling to rest on top of the four walls
    ceiling.setPosition(0,10,20)

    #add a meter marker at 0 meters along the z axis of the room
    #meter0 = vizshape.addPlane(size = (5,.3), axis = vizshape.AXIS_Y, cullFace = False)
    #meter0.setPosition(0,.3, 0)
    #makes the meter marker appear yellow
    #meter0.color(viz.WHITE)

    #adds a wall(plane) that when facing the frontWall is to the camera's back
    #wall is 35x10 meters**2
    #wall is rotated about the X axis(perpendicular to Z axis)
    BallPlane = vizshape.addPlane(size=(2,2), axis=vizshape.AXIS_Z, cullFace = False)
    #shifts the wall to match the edge of the floor
    BallPlane.setPosition(0,5,-20)
    #makes the wall appear white
    BallPlane.color(viz.WHITE)
    BallPlane.alpha(0.2)

    """</END OF DEFINING THE WORLD>"""

def FindIndex(Array,Value):
    
    Index =[]
    for index, number in enumerate(Array):
        if number == Value:
            Index.append(index)
        
    return Index

def ExtractDataFromMatFile(FileName):
    
    global  MarkerPos_XYZ_Matrix, F_Matrix, FrameNumberMatrix
    global View_Pos_XYZ, View_Quat_WXYZ, T, EventFlag
    global TrialEndIndex, TrialStartIndex;

    F_Matrix = np.array([], dtype = float);
    FrameNumberMatrix = np.array([], dtype = float);

    MarkerPos_XYZ_Matrix = np.array([],  dtype = float);

    MarkerPos_XYZ_Matrix.resize((1,24))
    
    TextFileName = '4-1'
    txtFile = open(TextFileName +'.txt',"r") # "exp_data-2014-5-9-13-9 copy.txt"
    i = 0

    print 'Parsing TextData in progress for',TextFileName,'.txt ....\n' 
    
    for aline in txtFile:
        Line = aline.split()
        for i in range(len(Line)):
            TempArray = np.array([]);
            #print 'TextFile Parsing'
            if (Line[i] == 'FrameTime'):
                F_Matrix = np.hstack((F_Matrix, float(Line[i+1])))
            elif (Line[i] == 'FrameNumber'):
                FrameNumberMatrix = np.hstack((FrameNumberMatrix, float(Line[i+1])));
            elif (Line[i] == 'MarkerPos_XYZ'):
                for j in range(24):
                    TempArray = np.hstack(( TempArray, float(Line[i+j+1])))
                MarkerPos_XYZ_Matrix = np.vstack((MarkerPos_XYZ_Matrix, TempArray ));

    txtFile.close()
    
    MarkerPos_XYZ_Matrix = np.delete(MarkerPos_XYZ_Matrix, 0, 0)
    MarkerPos_XYZ_Matrix = np.divide(MarkerPos_XYZ_Matrix, 1000);
    
    print 'Pos size=\n', MarkerPos_XYZ_Matrix.shape
    print 'T size=\n', F_Matrix.shape
    print 'F size=\n', FrameNumberMatrix.shape
    
    print '...Experiment Text File Parsing Done!!'


    TrialStartIndex = 1;
    TrialEndIndex = 1200;
    
    TrialNumber = (TrialEndIndex)
    print 'Number of Trials are=\n', TrialNumber
    print 'Start Indexes =', TrialStartIndex
    print 'End Indexes =', TrialEndIndex

def CreateVisualObjects():

    global MarkerObject;

    MarkerObject = [];
    ColorList = [viz.WHITE, viz.GREEN,viz.BLUE,viz.YELLOW,viz.BLACK,viz.PURPLE,viz.GRAY,viz.RED]
    for i in range(8):
        #creats a sphere(the ball) with radius of 1cm
        Sphere = vizshape.addSphere(radius = .01)
        #colors the ball red
 #      Sphere.color(ColorList[i])
        Sphere.color(viz.RED)
        Sphere.visible(True)
        MarkerObject.append(Sphere)
    
    Origin = vizshape.addAxes()
    Origin.setPosition(0,0,0)

def Quaternion2Matrix(Q):
    Q = Q/np.linalg.norm(Q); # Ensure Q has unit norm
    
    # Set up convenience variables
    x = Q[0]; y = Q[1]; z = Q[2]; w = Q[3];
    w2 = w*w; x2 = x*x; y2 = y*y; z2 = z*z;
    xy = x*y; xz = x*z; yz = y*z;
    wx = w*x; wy = w*y; wz = w*z;
    
    M = np.array([[w2+x2-y2-z2 , 2*(xy - wz) , 2*(wy + xz) ,  0],
         [ 2*(wz + xy) , w2-x2+y2-z2 , 2*(yz - wx) ,  0 ],
         [ 2*(xz - wy) , 2*(wx + yz) , w2-x2-y2+z2 ,  0 ],
         [     0      ,       0     ,       0     ,  1 ]], dtype = float);
    return M;

def SetRotationAngle(angle):
    global alpha;
    print 'Screen Rotation Set to ', angle;
    alpha = angle*(np.pi/180);

def onTimer(num):
#for counter in range(50,60):#TrialNumber):
    global counter, FrameNumber, GazeLine, LeftEyeShift, nearH, nearW, BallPlane, MarkerPos_XYZ_Matrix;
    global lEyeOffsetMatrix, lEyeRotationMatrix;
    global TrialStartIndex, TrialEndIndex;

    #StartIndex = TrialStartIndex[counter]
    #EndIndex   = TrialEndIndex[counter]
#    for FrameNumber in range(StartIndex, EndIndex):
    #print 'F=', FrameNumber,'P=', Ball_Pos_XYZ[:,FrameNumber],'Q=',View_Quat_WXYZ[:,FrameNumber], '\n'
   # print 'Pos ===> ', MarkerPos_XYZ_Matrix[FrameNumber,0:2]
    for i in range(8):
        MarkerObject[i].setPosition(*MarkerPos_XYZ_Matrix[FrameNumber,3*i:3*i+3])
        
    if FrameNumber < len(FrameNumberMatrix):
        FrameNumber = FrameNumber + 1
    else:
        viz.stop
        #time.sleep(1)


if __name__ == '__main__':
    
    global lEyeOffsetMatrix, lEyeRotationMatrix;
    global FrameNumber, counter, TrialStartIndex, TrialEndIndex;
    global nearW,nearH;
    
    TimeInterval = 0.027
    nearH = 1.2497;
    nearW = 1.5622;
    viz.setMultiSample(4)
    viz.fov(60)
    viz.go()
    ExtractDataFromMatFile('Exp_RawMat_MarkerPos2014-11-26-21-18.mat');
    CreateTheRoom()
    CreateVisualObjects()
    #sets where the camera view is located
    viz.MainView.setPosition([0.26132529973983765, 1.5138036012649536, 0.4800882935523987])
    #sets the angle at which the camera view is pointed
    viz.MainView.setEuler(3.1846203804016113, -0.0, 0.0)
    lEyeOffsetMatrix = np.array([[1, 0, 0, -0.03],[0, 1, 0, 0], [0, 0, 1, 0],[0, 0, 0, 1]], dtype = float)
    lEyeRotationMatrix = np.eye(4, dtype = float);
    counter = 1
    #FrameNumber = TrialStartIndex[0] - 100;
    FrameNumber = 1;
    SetRotationAngle(1.6);
    #if the timer goes off go to the onTimer function
    viz.callback(viz.TIMER_EVENT,onTimer)
    viz.starttimer(1, TimeInterval, viz.FOREVER)