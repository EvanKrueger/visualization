# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
#def __init__():
#    import theano
#    print(theano)

import numpy as np
import time
import sys
import pandas as pd
import cv2
import time, threading
import os
import _pickle as cPickle

def runRL(myImage):
    return np.random.random((1,3))+1, np.random.random((1,3))+1
#os.chdir('C:\Users\Kamran Binaee\Documents\Python Scripts\KamranWorksapce\DataGenerationVisualization')

def main():
    global counter
    #print(time.ctime())
    myThread = threading.Timer(.05, main)
    myThread.start()
    counter = counter + 1
    df = pd.read_pickle('RL_Data.pkl')
    if counter > 2000:
        print('\n\n RunTime Finish!\n\n')
        myThread.cancel()
        quit()
        return

   
    
    #print('read Data Flag', originalFile['ReadyToReadData'].values[0])
    #print('read Image Flag', originalFile['ReadyToReadImage'].values[0])
    if df['ReadyToReadImage'].values[0] == 0:
        #print('(',counter, ') RL: Image Not Ready ==> Skip!')
        return
    for key in df.keys():
        print(key, ' = ',df[key].values)
    print('\n\n')
    myImage = cv2.imread('ImageForRL.jpg')
    
    gazePos, handPose= runRL(myImage)
    df['Hand_X'].values[0] = handPose[0][0]
    df['Hand_Y'].values[0] = handPose[0][1]
    df['Hand_Z'].values[0] = handPose[0][2]
    df['Gaze_X'].values[0] = gazePos[0][0]
    df['Gaze_Y'].values[0] = gazePos[0][1]
    df['Gaze_Z'].values[0] = gazePos[0][2]
    df['ReadyToReadData'].values[0] = 1
    df['ReadyToReadImage'].values[0] = 0
    df.to_pickle('RL_Data.pkl')
    with open('RL_Data.pkl', 'wb') as pickle_file:
        cPickle.dump(df, pickle_file, protocol=2)
        pickle_file.close()
    print('RL Done!')


counter = 0
#df = pd.read_csv('RL_Data.csv')

#==============================================================================
# data = {'Hand_X': [1.5],
#             'Hand_Y': [1.5],
#             'Hand_Z': [1.5],
#             'Gaze_X': [1.5],
#             'Gaze_Y': [1.5],
#             'Gaze_Z': [1.5],
#             'ReadyToReadData': [1],
#             'ReadyToReadImage': [0]}
# originalFile = pd.DataFrame(data, columns = data.keys())
# 
#==============================================================================
#print('read Data Flag', originalFile['ReadyToReadData'].values[0])
#print('read Image Flag', originalFile['ReadyToReadImage'].values[0])
#for key in originalFile.keys():
#    print(key, ' = ',originalFile[key].values)
originalFile = pd.read_pickle('RL_Data.pkl')
originalFile['ReadyToReadData'].values[0] = 0
originalFile['ReadyToReadImage'].values[0] = 1

with open('RL_Data.pkl', 'wb') as pickle_file:
    cPickle.dump(originalFile, pickle_file, protocol=2)
    pickle_file.close()

#pickle.dump(originalFile, 'RL_Data.pkl', protocol=2)
#originalFile.to_pickle('RL_Data.pkl')

#df = pd.read_pickle('RL_Data.pkl')

#sys.path.append("C:\\Users\\Kamran Binaee\\gym")

main()
os.system('"C:\\Users\\Kamran Binaee\\Documents\\Python Scripts\\KamranWorksapce\\DataGenerationVisualization\\DrawMarkers.exe"')
