# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
from PIL import Image as im
import os
#import scipy.io as sio
import viz
import vizact
import vizshape
#import vizmatplottwo
#import scipy
import vizmenu
import time
import pandas as pd


class myRoom:
    def __init__(self):
        
        self.topLight = []
        self.sideLight = []
        self.floor = []
        self.frontWall = []
        self.leftWall = []
        self.rightWall = []
        self.backWall = []
        self.ballPlane = []
        self.ball = []
        self.paddle=[]
        self.cycEyeNode = []
        self.frameCounter = 0
        self.rawDataFrame = []
        self.processedDataFrame = []
        self.trialInfoDataFrame = []
        self.calibDataFrame = []
        self.attachViewPortToHead = False
        self.saveImagesForDataSet = False
        self.saveImagesForRL = True        
        self.includeBlankDuration = False
        self.renderGaze = False
        
def readPickleFile(room, fileName):
    df = pd.read_pickle(fileName)
    
    room.rawDataFrame = df['raw']
    room.processedDataFrame = df['processed']
    room.calibDataFrame = df['calibration']
    room.trialInfoDataFrame = df['trialInfo']
    
    return room
    
    
    return dataFrame

def CreateTheRoom(room):

    """<START OF DEFINING THE WORLD>"""
    
    #disables the original light
    viz.disable(viz.LIGHT0)

    room.topLight = viz.addLight() 
    room.topLight.setPosition(0,4,0)
    room.topLight.setEuler( -45, 90 ,0 )
    #topLight.direction(0,-1,0) 
    room.topLight.spread(270) 
    room.topLight.intensity(2) 
    #topLight.spotexponent(2)     
    room.sideLight = viz.addLight()
    room.sideLight.setPosition(0,1,0)
    room.sideLight.setEuler(45, 0,-90)
    room.sideLight.spread(270) 
    room.sideLight.intensity(2) 

    #creates the floor plane to be 35x80
    #rotated around the Z axis(perpendicular to Y axis)
    room.floor = vizshape.addPlane(size=(35.0,80.0), axis=vizshape.AXIS_Y, cullFace=False)
    #makes the floor look like wood
    room.floor.texture(viz.addTexture('images/tile_wood.jpg'))
    #moves the floor +20 meters in the z direction
    room.floor.setPosition(0,0,20)

    """
    Could use Gabe's mocap interaface here to create a room easily
    and play with textures. 

    If performance is an issue, set cullFace to True.
    """
    #adds the wall(plane) farthest from the hand/person
    #35x10 meters**2
    #rotated around the X axis (perpendicular to Z axis)
    room.frontWall = vizshape.addPlane(size=(35,10), axis=-vizshape.AXIS_Z, cullFace = False)
    #makes the front wall look like wall
    room.frontWall.texture(viz.addTexture('images/tile_slate.jpg'))
    #moves the wall to match the edge of the 
    room.frontWall.setPosition(0,5,60)
    #makes the wall appear white
    room.frontWall.color(viz.GRAY)
    

    #adds the wall(plane) that when facing the frontWall, is to the camera's left
    #wall is 80x10 meters**2
    #wall is rotated about the Y axis(perpendicular to X axis)
    room.leftWall = vizshape.addPlane(size=(80,10), axis=-vizshape.AXIS_X, cullFace = False)
    #makes the left wall look like wall
    room.leftWall.texture(viz.addTexture('images/tile_slate.jpg'))
    #shifts the wall to match the edge of the floor
    room.leftWall.setPosition(-17.5,5,20)
    #makes the wall appear white
    room.leftWall.color(viz.GRAY)

    #adds a wall(plane) that when facing the frontWall is to the camera's right
    #wall is 80x10 meters**2
    #wall is rotated about the Y axis(perpendicular to X axis)
    room.rightWall = vizshape.addPlane(size=(80,10), axis=-vizshape.AXIS_X, cullFace = False)
    #makes the right wall look like wall
    room.rightWall.texture(viz.addTexture('images/tile_slate.jpg'))
    #shifts the wall to match the edge of the floor
    room.rightWall.setPosition(17.5,5,20)
    #makes the wall appear white
    room.rightWall.color(viz.GRAY)

    #adds a wall(plane) that when facing the frontWall is to the camera's back
    #wall is 35x10 meters**2
    #wall is rotated about the X axis(perpendicular to Z axis)
    room.backWall = vizshape.addPlane(size=(35,10), axis=vizshape.AXIS_Z, cullFace = False)
    #shifts the wall to match the edge of the floor
    room.backWall.setPosition(0,5,-20)
    #makes the wall appear white
    room.backWall.color(viz.WHITE)

    #adds a ceiling(plane) that when facing the frontWall is to the camera's topside
    #wall is 35x80 meters**2
    #wall is rotated about the Z axis(perpendicular to Y axis)
    room.ceiling = vizshape.addPlane(size=(35.0,80.0), axis=vizshape.AXIS_Y, cullFace=False)
    #makes the ceiling appear Skyblue in color
    room.ceiling.color(viz.SKYBLUE)
    #shifts the ceiling to rest on top of the four walls
    room.ceiling.setPosition(0,10,20)

    #add a meter marker at 0 meters along the z axis of the room
    #meter0 = vizshape.addPlane(size = (5,.3), axis = vizshape.AXIS_Y, cullFace = False)
    #meter0.setPosition(0,.3, 0)
    #makes the meter marker appear yellow
    #meter0.color(viz.WHITE)

    #adds a wall(plane) that when facing the frontWall is to the camera's back
    #wall is 35x10 meters**2
    #wall is rotated about the X axis(perpendicular to Z axis)
    room.ballPlane = vizshape.addPlane(size=(2,2), axis=vizshape.AXIS_Z, cullFace = False)
    #shifts the wall to match the edge of the floor
    room.ballPlane.setPosition(0,5,-20)
    #makes the wall appear white
    room.ballPlane.color(viz.WHITE)
    room.ballPlane.alpha(0.2)
    
    room.ball = vizshape.addSphere(radius = 0.03, color = viz.RED)
    room.paddle = vizshape.addCylinder(height = 0.05, radius = 0.15, color = viz.YELLOW, axis = vizshape.AXIS_Z)
    room.cycEyeNode = vizshape.addCone(radius = 0.05, height = 0.17, color = viz.PURPLE, axis = vizshape.AXIS_Z)
    if ( room.renderGaze == True ):
        room.gazePoint = vizshape.addSphere(radius = 0.0003, color = viz.GREEN)
        room.gazePoint.setParent(room.cycEyeNode)
    
    return room

    """</END OF DEFINING THE WORLD>"""

def FindIndex(Array,Value):
    
    Index =[]
    for index, number in enumerate(Array):
        if number == Value:
            Index.append(index)
        
    return Index


def CreateVisualObjects(room):

    global MarkerObject;

    MarkerObject = [];
    ColorList = [viz.WHITE, viz.GREEN,viz.BLUE,viz.YELLOW,viz.BLACK,viz.PURPLE,viz.GRAY,viz.RED]
    
    room.origin = vizshape.addAxes()
    room.origin.setPosition(0,0,0)

def Quaternion2Matrix(Q):
    Q = Q/np.linalg.norm(Q); # Ensure Q has unit norm
    
    # Set up convenience variables
    x = Q[0]; y = Q[1]; z = Q[2]; w = Q[3];
    w2 = w*w; x2 = x*x; y2 = y*y; z2 = z*z;
    xy = x*y; xz = x*z; yz = y*z;
    wx = w*x; wy = w*y; wz = w*z;
    
    M = np.array([[w2+x2-y2-z2 , 2*(xy - wz) , 2*(wy + xz) ,  0],
         [ 2*(wz + xy) , w2-x2+y2-z2 , 2*(yz - wx) ,  0 ],
         [ 2*(xz - wy) , 2*(wx + yz) , w2-x2-y2+z2 ,  0 ],
         [     0      ,       0     ,       0     ,  1 ]], dtype = float);
    return M;

def SetRotationAngle(angle):
    global alpha;
    print 'Screen Rotation Set to ', angle;
    alpha = angle*(np.pi/180);

def onTimer(num):
#for counter in range(50,60):#TrialNumber):
    global counter, FrameNumber, GazeLine, LeftEyeShift, nearH, nearW, ballPlane, MarkerPos_XYZ_Matrix;
    global lEyeOffsetMatrix, lEyeRotationMatrix;
    global TrialStartIndex, TrialEndIndex;
    global room
    
    df = pd.read_pickle("C:\\Users\\Kamran Binaee\\Documents\\Python Scripts\\KamranWorksapce\\DataGenerationVisualization\\RL_Data.pkl")
    print 'Flag = ',df['ReadyToReadData'].values[0]
    #print df.columns
    #print df
    if (df['ReadyToReadData'].values[0] == 0):
        print 'Vizard: Data Not Ready ==> Skip!\n\n'
        room.paddle.color(viz.YELLOW)
        return
    else:
        print '\n\nYESSSSSSSSSSS'

    room.cycEyeNode.setPosition(*room.rawDataFrame.viewPos.values[room.frameCounter])
    room.cycEyeNode.setQuat(*room.rawDataFrame.viewQuat.values[room.frameCounter])
    room.ball.setPosition(*room.rawDataFrame.ballPos.values[room.frameCounter])
    if ( room.includeBlankDuration == True ):
        if (room.rawDataFrame.isBallVisibleQ.values[room.frameCounter]):
            room.ball.alpha(1)
        else:
            room.ball.alpha(0)
    #room.paddle.setPosition(*room.rawDataFrame.paddlePos.values[room.frameCounter])
    room.paddle.setPosition(*[df['Hand_X'].values[0], df['Hand_Y'].values[0], df['Hand_Z'].values[0]])
    room.paddle.setQuat(*room.rawDataFrame.paddleQuat.values[room.frameCounter])
    room.frameCounter = room.frameCounter + 1
    if (room.attachViewPortToHead == True) :
        viz.MainView.setPosition(*room.rawDataFrame.viewPos.values[room.frameCounter])
        viz.MainView.setQuat(*room.rawDataFrame.viewQuat.values[room.frameCounter])
    if ( room.renderGaze):
        room.gazePoint.setPosition(*room.processedDataFrame.rotatedGazePoint.values[room.frameCounter])
    #print 'Ball Position = ', room.ball.getPosition()
    
    if ( room.saveImagesForDataSet == True):
        viz.window.screenCapture("C:\\Users\\Kamran Binaee\\Documents\\Python Scripts\\KamranWorksapce\\DataGenerationVisualization\\2016-4-19-14-4\\Images\\"+ str(room.frameCounter) + '.jpg')

    if ( room.saveImagesForRL == True):
        viz.window.screenCapture('ImageForRL.jpg')
        df['ReadyToReadImage'].values[0] = 1
        df['ReadyToReadData'].values[0] = 0
        df.to_pickle("C:\\Users\\Kamran Binaee\\Documents\\Python Scripts\\KamranWorksapce\\DataGenerationVisualization\\RL_Data.pkl")
        #room.paddle.color(viz.GREEN)
        print 'Yay'

    

if __name__ == '__main__':
    
    global lEyeOffsetMatrix, lEyeRotationMatrix;
    global FrameNumber, counter, TrialStartIndex, TrialEndIndex;
    global nearW,nearH;
    global rawDataFrame, processedDataFrame, calibDataFrame, trialInfoDataFrame
    
    TimeInterval = 0.05
    nearH = 1.2497;
    nearW = 1.5622;
    viz.setMultiSample(4)
    viz.fov(60)
    viz.go()
    #ExtractDataFromMatFile('Exp_RawMat_MarkerPos2014-11-26-21-18.mat');
    fileName = "C:\\Users\\Kamran Binaee\\Documents\\Python Scripts\\KamranWorksapce\\DataGenerationVisualization\\2016-4-19-14-4\\exp_data-2016-4-19-14-4.pickle"
    room = myRoom()
    room = readPickleFile(room, fileName)
    room = CreateTheRoom(room)
    #CreateVisualObjects()
    
    print 'Running Visualization Code ...\n\n\n'
    print 'Attach View to Head = ', room.attachViewPortToHead
    print 'Save Images for RL = ', room.saveImagesForRL
    print 'Save Images for Data Set = ', room.saveImagesForDataSet
    print 'Ball Disappears = ', room.includeBlankDuration, '\n'
    if (room.attachViewPortToHead == False) :
        #sets where the camera view is located
        viz.MainView.setPosition([-0.46132529973983765, 1.8138036012649536, -1.4800882935523987])
        #sets the angle at which the camera view is pointed
        viz.MainView.setEuler(3.1846203804016113, -0.0, 0.0)
    #lEyeOffsetMatrix = np.array([[1, 0, 0, -0.03],[0, 1, 0, 0], [0, 0, 1, 0],[0, 0, 0, 1]], dtype = float)
    #lEyeRotationMatrix = np.eye(4, dtype = float);
    #counter = 1
    #FrameNumber = TrialStartIndex[0] - 100;
    #FrameNumber = 1;
    #SetRotationAngle(1.6);
    #if the timer goes off go to the onTimer function
    viz.callback(viz.TIMER_EVENT, onTimer)
    viz.starttimer(1, TimeInterval, viz.FOREVER)